const variable = process.env.NODE_ENV;

module.exports = function wrapperImage(element) {
	if (variable === "production") {
		return (`app/${element}`);
	}
	return element;
}
