import favicon from "./favicon.png";
import giftBox from "./giftBox.png";
import logo from "./logo.png";
import ribbon from "./ribbon.png";


export {
	favicon,
	giftBox,
	logo,
	ribbon
};
