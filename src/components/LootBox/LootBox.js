import React from 'react';
import { bool, number, func } from "prop-types";

import { CSSTransition } from 'react-transition-group';

import classnames from "classnames";
import { giftBox } from "../../images/index";

import classes from "./LootBox.less";


class LootBox extends React.Component {
	static propTypes = {
		funBalance: number,
		lootBoxVisibility: bool,
		showLootBox: func,
		hideLootBox: func
	}

	state = {
		onExitingAnimation: false
	}

	componentDidMount() {
		setTimeout(() => {
			if (this.props.location.hash === "" && !this.state.onExitingAnimation) {
				this.props.hideLootBox();
				this.setState({
					onExitingAnimation: true,
				},() => {
					this.setState({
						onExitingAnimation: false,
					});

					this.props.showLootBox();
				});
			}
		},300);
	}
	// 	const dots = [
	// 		{ y: 100, width: 100, x: 0, element: this.onLoadAnimation.current },
	// 		{ y: 0, width: 100, x: 0, element: this.onLoadAnimation.current }
	// 	];
	//
	// 	const time = 400;
	//
	// 	const update = ({ y, width, x, element }) => {
	// 		element.style.display = "flex";
	// 		element.style.transform = `translateY(${y}%) translateX(${x}%)`;
	// 		element.style.width = `${width}%`;
	// 	};
	//
	// 	this.setState({
	// 		odometerValue: this.props.funBalance ? this.props.funBalance : 0
	// 	});
	//
	// 	customTween(dots, time, update, this.onLoadAnimation.current);
	// }
	//
	// componentDidUpdate(prevProps) {
	// 	//Up
	// 	if (this.props.lootBoxVisibility && this.state.odometerValue !== this.props.funBalance && this.props.funBalance) {
	// 		const dots = [
	// 			{ y: 100, width: 100, x: 0, element: this.onLoadAnimation.current },
	// 			{ y: 0, width: 100, x: 0, element: this.onLoadAnimation.current }
	// 		];
	//
	// 		const time = 400;
	//
	// 		const update = ({ y, width, x, element }) => {
	// 			element.style.display = "flex";
	// 			element.style.transform = `translateY(${y}%) translateX(${x}%)`;
	// 			element.style.width = `${width}%`;
	// 		};
	// 		this.setState({
	// 			odometerValue: this.props.funBalance ? this.props.funBalance : 0
	// 		});
	//
	// 		customTween(dots, time, update, this.onLoadAnimation.current);
	// 	} else if (!this.props.lootBoxVisibility && this.state.odometerValue !== 0 && this.props.funBalance) {
	// 		//down
	// 		const dots = [
	// 			{ y: 0, width: 100, x: 0, display: 0, element: this.onLoadAnimation.current },
	// 			{ y: 100, width: 100, x: 0, display: 100, element: this.onLoadAnimation.current },
	// 		];
	//
	// 		const time = 600;
	//
	// 		const update = ({ y, width, x, display, element }) => {
	// 			element.style.transform = `translateY(${y}%) translateX(${x}%)`;
	// 			element.style.width = `${width}%`;
	//
	// 			if (display === 100) {
	// 				element.style.display = `none`;
	// 			}
	// 		};
	// 		this.setState({
	// 			odometerValue: 0
	// 		});
	//
	// 		customTween(dots, time, update, this.onLoadAnimation.current);
	// 	}
	// /**
	//  * starts on first render when lootbox is open
	//  */
	// else if (prevProps.lootBoxVisibility  && this.props.lootBoxVisibility && this.props.funBalance && this.state.odometerValue !== this.props.funBalance) {
	// 	this.setState({
	// 		odometerValue: this.props.funBalance
	// 	});
	// }
	// }

	render() {
		const { lootBoxVisibility } = this.props;

		const { onExitingAnimation } = this.state;

		const showMenuClasses = classnames({
			[classes.hide]: !onExitingAnimation
		});
		return (
			<div
				className={showMenuClasses}
			>
				<CSSTransition
					in={lootBoxVisibility} timeout={600} classNames={"fade-up"} onEnter={() => {
						this.setState({
							onExitingAnimation: true
						});
					}} onExited={() => {
						this.setState({
							onExitingAnimation: false
						});
					}}
				>
					<div className={classes.lootBox}>
						<div className={classes.lootBoxContent}>
							<div className={classes.lootBoxImageWrapper}>
								<img
									className={classes.lootBoxImage}
									src={wrapperImage(giftBox)}
									alt="lootBox"
								/>
							</div>

							<div className={classes.lootBoxFooter}>
								<div className={classes.lootBoxOpenLootBox}>
									<p className={classes.lootBoxOpenLootBoxText}> Your Message Comes Here</p>
									<a className={classes.lootBoxOpenLootBoxButton}>
										Get More
									</a>
								</div>
								{/*<div className={classes.lootBoxCounterWrapper}>*/}
								{/*<p>You Have </p>*/}
								{/*/!*<img src={CounterExample} className={classes.lootBoxCounterImage} alt=""/>*!/*/}
								{/*<Odometer*/}
								{/*value={odometerValue}*/}
								{/*format="(,ddd)"*/}
								{/*classes={classes.lootBoxOdometerStyle}*/}
								{/*/>*/}
								{/*<p> Loot Boxes </p>*/}
								{/*</div>*/}
							</div>
						</div>
					</div>
				</CSSTransition>
			</div>
		);
	}
}

export default LootBox;
