import React, { Fragment } from 'react';
import { func, object, array, bool, any, string } from "prop-types";
import classnames from "classnames";

import TabsMaterial from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import classes from "./Tabs.less";


class Tabs extends React.Component {
	static propTypes = {
		customstyle: object,
		fullWidth: bool,
		labels: array,
		mainTabChange: func,
		tabname: string,
		value: any
	}

	static defaultProps = {
		fullWidth: false
	}
	render() {
		const { labels, fullWidth, customstyle, value } = this.props;


		return (
			<Fragment>
				<TabsMaterial
					classes={{ flexContainer: classes.tab, indicator: classes.hide, scroller: classes.scroller }}
					style={customstyle}
					fullWidth={fullWidth}
					value={value}
					indicatorColor="primary"
					textColor="primary"
					{...this.props}
				>
					{
						labels.map((label, index) => {
							const tabClasses = classnames({
								[classes.tabSingleTab]: true,
								[classes.tabActive]: value !== false && value === index
							});

							return (
								<Tab
									color="secondary"
									disableRipple
									focusRippleColor="darkRed"
									touchRippleColor="red"
									className={tabClasses}
									key={label.name}
									label={
										<span
											style={
												label.iconStyle &&
												label.iconStyle
											}
										>{ label.name }</span>}
									icon={label.icon ? <img src={wrapperImage(label.icon)}  alt="" /> : false}
								/>
							);
						})
					}
				</TabsMaterial>
			</Fragment>
		);
	}
}

export default Tabs;
